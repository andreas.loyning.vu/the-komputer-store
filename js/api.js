const BASE_URL = 'http://localhost:3000';

function extractBodyfromResp(response) {
    return response.json();
}

export function fetchComputers() {
    return fetch(`${BASE_URL}/computers`)
        .then(extractBodyfromResp)
        .catch(console.error);
}

export function fetchComputer(id) {
    return fetch(`${BASE_URL}/computers/${id}`)
        .then(extractBodyfromResp)
        .catch(console.error);
}