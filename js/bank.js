export class Bank {

    // DOM elements
    elBankBalance = document.getElementById('bank-balance');
    elOutstandingLoanBalance = document.getElementById('outstanding-loan-balance');
    elOutstandingLoanDiv = document.getElementById('container-outstanding-loan');

    // Data properties
    hasLoan = false;
    bankBalance = 10000;
    outstandingLoan = 0;
    canTakeLoan = true;
    constructor() {
        this.render();
    }

    grantLoan(amount) {
        if (
            this.hasLoan 
            || !this.canTakeLoan
            || amount > this.bankBalance * 2
            || amount <= 0
            || amount.match('^[0-9]*$') == null
            ) {
            return false;
        }
        
        this.bankBalance += +amount;
        this.outstandingLoan = +amount;
        this.hasLoan = true;
        this.canTakeLoan = false;
        this.render();
        return true;
    }

    addBalanceAmount(amount) {
        this.bankBalance += amount;
        this.render();
    }

    addLoanAmount(amount) {
        this.outstandingLoan += amount;
        this.render();
    }

    getBankBalance() {
        return this.bankBalance;
    }

    getOutstandingLoan() {
        return this.outstandingLoan;
    }

    setOutstandingLoan(amount) {
        this.outstandingLoan = amount;
        this.render();
    }

    setCanTakeLoan(bool) {
        this.canTakeLoan = bool;
    }

    setHasLoan(boolean) {
        this.hasLoan = boolean;
        this.render();
    }

    render() {
        this.elBankBalance.innerText = this.getBankBalance() + ' kr';

        if (this.hasLoan) {
            this.elOutstandingLoanDiv.style = 'display: ?;';
            this.elOutstandingLoanBalance.innerText = this.outstandingLoan + ' kr';
        } else {
            this.elOutstandingLoanDiv.style = 'display: none;';
        }
    }
}