export class Work {
    // DOM elements
    elPayBalance = document.getElementById('pay-balance');
    elBtnRepay = document.getElementById('btn-repay');
    showRepay = false;
    
    
    // Data properties
    pay = 0;
    constructor() {
        this.render();
    }

    bankMoney(bank) {
        if (bank.hasLoan) {
            let deduction = Math.floor(this.pay * 0.1);
            this.repayAmount(deduction, bank);
            bank.addBalanceAmount(this.pay - deduction);
        } else {
            bank.addBalanceAmount(this.pay);
        }

        this.pay = 0;
        this.render();
    }

    doWork() {
        this.pay += 100;
        this.render();
    }

    repayLoan(bank) {
        this.pay = this.repayAmount(this.pay, bank);
        this.render();
    }

    setShowRepay(bool) {
        this.showRepay = bool;
        this.render();
    }

    repayAmount(amount, bank) {
        let remainingLoan = bank.getOutstandingLoan();
        if (amount >= remainingLoan) {
            amount -= remainingLoan
            bank.setOutstandingLoan(0);
            bank.setHasLoan(false);
            this.showRepay = false;
            return amount;
        } else {
            bank.addLoanAmount(-amount);
            return 0;
        }
    }

    render() {
        this.elPayBalance.innerText = this.pay + 'kr';

        if (this.showRepay) {
            this.elBtnRepay.style.display = 'block';
        } else {
            this.elBtnRepay.style.display = 'none';
        }
    }
}