import { MoneyController } from './moneyController.js';
import { Computer } from './computer.js';

class App {
    moneyController = new MoneyController();
    computerController = null;
    
    constructor() {}

    async init() {
        try {
            this.moneyController.init();
            this.computerController = await new Computer(this.moneyController).init();
        } catch(e) {
            console.error(e);
        }        
    };
}
// Initialize the app.
new App().init();