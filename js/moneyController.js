import { Bank } from './bank.js';
import { Work } from './work.js'


export class MoneyController {
    // DOM Elements
    elBtnBank = document.getElementById('btn-bank');
    elBtnWork = document.getElementById('btn-work');
    elBtnRepay = document.getElementById('btn-repay');
    elBtnLoan = document.getElementById('btn-loan');
    

    bank = new Bank();
    job = new Work();
    
    constructor() {

    }

    init() {
        this.createEventListeners();
    }

    createEventListeners() {
        this.elBtnWork.addEventListener('click', () => {
            this.job.doWork();
        });
        //  For banking the money
        this.elBtnBank.addEventListener('click', () => {
            this.job.bankMoney(this.bank);
        });
        // For downpaying the loan directly
        this.elBtnRepay.addEventListener('click', () => {
            this.job.repayLoan(this.bank);
        });
        // For taking a loan
        this.elBtnLoan.addEventListener('click', () => {
            let selectedAmount = prompt('How much?', '0');
            if (selectedAmount != null) {
                let wasGrantedLoan = this.bank.grantLoan(selectedAmount);
                if (!wasGrantedLoan) {
                    alert("Error: You either is not eligible for a loan or you asked for too much.");
                    return;
                }

                this.job.setShowRepay(true);
            }
        });
    }

    buyComputer(computer) {
        if (this.bank.getBankBalance() >= computer.price) {
            this.bank.addBalanceAmount(-computer.price);
            this.bank.setCanTakeLoan(true);
            return true;
        } 

        return false;
    }
}   