import { fetchComputers } from './api.js';

export class Computer {
    elComputerFeatures = document.getElementById('p-laptop-features');
    elComputerPicture = document.getElementById('selected-computer-image');
    elComputerDescription = document.getElementById('selected-computer-desc');
    elComputerTitle = document.getElementById('selected-computer-title');
    elComputerPrice = document.getElementById('selected-computer-cost');
    elComputerDropdown = document.getElementById('computer-dropdown');
    elBtnBuyComputer = document.getElementById('btn-buy-computer');

    computers = [];
    selectedComputer = null;

    constructor(moneyController) {
        this.moneyController = moneyController;
    }

    async init() {
        try {
            this.computers = await fetchComputers();
            this.createEventListeners();
            this.elComputerDropdown.disabled = false;
        } catch (e) {
            console.error(e);
        }

        this.computers.forEach(computer => {
            const newOption = new Option(computer.name, computer.id);
            this.elComputerDropdown.appendChild(newOption);
        });

        this.render();
    }

    createEventListeners() {
        this.elComputerDropdown.addEventListener('change', () => {
            const foundComputer = this.computers.find(computer => computer.id == this.elComputerDropdown.value);
            this.changeComputerView(foundComputer);
        });

        this.elBtnBuyComputer.addEventListener('click', this.onBuyComputer.bind(this));
    }

    onBuyComputer() {
        if (this.selectedComputer === null) {
            return;
        }
        if (this.moneyController.buyComputer(this.selectedComputer)) {
            this.render()
            alert(`You bought the computer ${this.selectedComputer.name}!`);
            return;
        }

        alert('You do not have enough money!');
    }

    render() {
        if (this.selectedComputer !== null) {
            this.elComputerPicture.src = this.selectedComputer.image;
            this.elComputerDescription.innerText = this.selectedComputer.description;
            this.elComputerTitle.innerText = this.selectedComputer.name;
            this.elComputerPrice.innerText = this.selectedComputer.price + ' kr';
            this.elComputerFeatures.innerText = this.selectedComputer.features.join('\n')
        }
    }

    changeComputerView(newComputer) {
        if (newComputer === null) {
            return;
        }

        this.selectedComputer = newComputer;
        console.log(this.selectedComputer);
        this.render();
    };
}